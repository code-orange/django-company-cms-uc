from django.utils.translation import gettext as _


def uc_hosted_exchange_product_comparison():
    available_products = list()

    # HSUCPLAN3
    available_products.append(
        {
            "name": _("UC-Plan 3"),
            "price": "13.60 Euro" + " " + _("(monthly)"),
            "text": _(
                "Contains UC-Plan 2 as well as Dolphin Team / Meet. Contains 10GB Dolphin Fileshare."
            ),
        }
    )

    # HSUCPLAN2
    available_products.append(
        {
            "name": _("UC-Plan 2 (Groupware only)"),
            "price": "9.60 Euro" + " " + _("(monthly)"),
            "text": _(
                "Contains UC-Plan 1 but with 25GB instead of 10GB. Contains 5GB Dolphin Fileshare."
            ),
        }
    )

    # HSUCPLAN1
    available_products.append(
        {
            "name": _("UC-Plan 1 (Groupware only)"),
            "price": "7.60 Euro" + " " + _("(monthly)"),
            "text": _(
                "E-Mail Mailbox with 10GB, Tasks, Calendars, Contacts and Notes. Also contains 2GB Dolphin Fileshare."
            ),
        }
    )

    return available_products


def uc_dolphin_meet_product_comparison():
    available_products = list()

    # HSUCPLAN3
    available_products.append(
        {
            "name": _("UC-Plan 3"),
            "price": "12.60 Euro" + " " + _("(monthly)"),
            "text": _(
                "Contains Hosted Exchange with 25GB as well as Dolphin Team / Meet. Contains 10GB Dolphin Fileshare."
            ),
        }
    )

    return available_products


def uc_dolphin_team_product_comparison():
    available_products = list()

    # HSUCPLAN3
    available_products.append(
        {
            "name": _("UC-Plan 3"),
            "price": "12.60 Euro" + " " + _("(monthly)"),
            "text": _(
                "Contains Hosted Exchange with 25GB as well as Dolphin Team / Meet. Contains 10GB Dolphin Fileshare."
            ),
        }
    )

    return available_products


def uc_email_signature_product_comparison():
    available_products = list()

    # MP-C1001524
    available_products.append(
        {
            "name": _("Letsignit Business"),
            "price": "1.20 Euro" + " " + _("(monthly)"),
            "text": _("Unlimited signatures per user."),
        }
    )

    return available_products
