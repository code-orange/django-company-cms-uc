from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="uc"),
    path("dolphin-meet", views.dolphin_meet, name="uc_dolphin_meet"),
    path("dolphin-team", views.dolphin_team, name="uc_dolphin_team"),
    path("e-fax", views.e_fax, name="uc_e_fax"),
    path("email-archiving", views.email_archiving, name="uc_email_archiving"),
    path("hosted-exchange", views.hosted_exchange, name="uc_hosted_exchange"),
    path("sms-gateway", views.sms_gateway, name="uc_sms_gateway"),
    path("email-signature", views.email_signature, name="uc_email_signature"),
]
