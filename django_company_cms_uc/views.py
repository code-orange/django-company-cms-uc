from math import ceil

from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
)
from django_company_cms_uc.django_company_cms_uc.products import *


def home(request):
    template = loader.get_template("django_company_cms_uc/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("What is that?")

    template_opts["product_uc_home_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_uc_home_01_header"
    ).content

    template_opts["product_uc_home_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_uc_home_01_content"
    ).content

    product_list = list()

    uc_product_filter_1 = "product_uc_home_item_"
    uc_product_filter_2 = "_name"

    uc_products = CompanyCmsGeneral.objects.filter(
        name__startswith=uc_product_filter_1,
        name__endswith=uc_product_filter_2,
    )

    for uc_product in uc_products:
        key = uc_product.name[: len(uc_product.name) - len(uc_product_filter_2)]

        product_list.append(
            {
                "name": CompanyCmsGeneral.objects.get(name=key + "_name").content,
                "subtitle": CompanyCmsGeneral.objects.get(
                    name=key + "_subtitle"
                ).content,
                "icon": CompanyCmsGeneral.objects.get(name=key + "_icon").content,
            }
        )

    template_opts["product_list"] = product_list

    return HttpResponse(template.render(template_opts, request))


def dolphin_meet(request):
    template = loader.get_template("django_company_cms_uc/dolphin_meet.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Dolphin Meet")

    template_opts["product_dolphin_meet_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_meet_01_header"
    ).content

    template_opts["product_dolphin_meet_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_meet_01_content"
    ).content

    template_opts["product_comparison_title"] = _("These products contain Dolphin Meet")
    template_opts["product_comparison_subtitle"] = _(
        "You can always upgrade to receive more features."
    )

    available_products = uc_dolphin_meet_product_comparison()

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))


def dolphin_team(request):
    template = loader.get_template("django_company_cms_uc/dolphin_team.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Dolphin Team")

    template_opts["product_dolphin_team_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_team_01_header"
    ).content

    template_opts["product_dolphin_team_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_team_01_content"
    ).content

    template_opts["product_dolphin_team_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_team_02_header"
    ).content

    template_opts["product_dolphin_team_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_dolphin_team_02_content"
    ).content

    template_opts["product_comparison_title"] = _("These products contain Dolphin Team")
    template_opts["product_comparison_subtitle"] = _(
        "You can always upgrade to receive more features."
    )

    available_products = uc_dolphin_team_product_comparison()

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))


def e_fax(request):
    template = loader.get_template("django_company_cms_uc/e_fax.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("E-Fax")

    template_opts["product_e_fax_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_01_header"
    ).content

    template_opts["product_e_fax_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_01_content"
    ).content

    template_opts["product_e_fax_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_02_header"
    ).content

    template_opts["product_e_fax_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_02_content"
    ).content

    template_opts["product_e_fax_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_03_header"
    ).content

    template_opts["product_e_fax_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_e_fax_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def email_archiving(request):
    template = loader.get_template("django_company_cms_uc/email_archiving.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("E-Mail Archiving")

    template_opts["product_email_archiving_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_email_archiving_01_header"
    ).content

    template_opts["product_email_archiving_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_email_archiving_01_content"
    ).content

    template_opts["product_email_archiving_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_email_archiving_02_header"
    ).content

    template_opts["product_email_archiving_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_email_archiving_02_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def hosted_exchange(request):
    template = loader.get_template("django_company_cms_uc/hosted_exchange.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Hosted Exchange")

    template_opts["product_hosted_exchange_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_hosted_exchange_01_header"
    ).content

    template_opts["product_hosted_exchange_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_hosted_exchange_01_content"
    ).content

    template_opts["product_hosted_exchange_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_hosted_exchange_02_header"
    ).content

    template_opts["product_hosted_exchange_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_hosted_exchange_02_content"
    ).content

    template_opts["product_comparison_title"] = _(
        "These products contain Hosted Exchange"
    )
    template_opts["product_comparison_subtitle"] = _(
        "You can always upgrade to receive more features."
    )

    available_products = uc_hosted_exchange_product_comparison()

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))


def sms_gateway(request):
    template = loader.get_template("django_company_cms_uc/sms_gateway.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("SMS Gateway")

    template_opts["product_sms_gateway_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_sms_gateway_01_header"
    ).content

    template_opts["product_sms_gateway_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_sms_gateway_01_content"
    ).content

    template_opts["product_sms_gateway_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_sms_gateway_02_header"
    ).content

    template_opts["product_sms_gateway_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_sms_gateway_02_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def email_signature(request):
    template = loader.get_template("django_company_cms_uc/email_signature.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Email Signature")

    template_opts["product_email_signature_01_header"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_01_header"
    ).content

    template_opts["product_email_signature_01_content"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_01_content"
    ).content

    template_opts["product_email_signature_02_header"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_02_header"
    ).content

    template_opts["product_email_signature_02_content"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_02_content"
    ).content

    template_opts["product_email_signature_03_header"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_03_header"
    ).content

    template_opts["product_email_signature_03_content"] = CompanyCmsGeneral.objects.get(
        name="product_email_signature_03_content"
    ).content

    template_opts["product_comparison_title"] = _("Licensing")
    template_opts["product_comparison_subtitle"] = _("User based licensing.")

    available_products = uc_email_signature_product_comparison()

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))
