from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class UcStaticViewSitemap(Sitemap):
    def items(self):
        return [
            "uc",
            "uc_dolphin_meet",
            "uc_dolphin_team",
            "uc_e_fax",
            "uc_email_archiving",
            "uc_hosted_exchange",
            "uc_sms_gateway",
            "email_signature",
        ]

    def location(self, item):
        return reverse(item)
